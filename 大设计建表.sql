USE Medicine2017;
IF OBJECT_ID ('tb_Administrator') IS NOT NULL
DROP TABLE tb_Administrator;
GO 
CREATE TABLE tb_Administrator
(AdminiNo
   CHAR(5)
   NOT NULL
   CONSTRAINT pk_Administrator_AdminiNo
   PRIMARY KEY(AdminiNo)
   CONSTRAINT ck_Administrator_AdminiNo
   CHECK(AdminiNo LIKE '[a-z][0-9][0-9][0-9][0-9]')
,AdminiName
 		VARCHAR(10) 								
		NOT NULL								
,ID
        VARCHAR(18)
		NOT NULL
,Gender									
		BIT								
		NOT NULL	
		
,Old
       CHAR(5)
,BirthDate									
		DATE								
		CONSTRAINT ck_Administrator_BirthDate								
	    CHECK (BirthDate BETWEEN '1960-01-01' AND '1999-12-31')		
,WorkDate
        DATE									
,Tel
        char(11)
,WarehouseNo
        char(7)
		NOT NULL
		CONSTRAINT fk_Administrator_WarehouseNo					
	       FOREIGN KEY(WarehouseNo)				
	       REFERENCES tb_Warehouse(WarehouseNo) 																												
)
IF OBJECT_ID ('tb_Production') IS NOT NULL
DROP TABLE tb_Production;
GO 
CREATE TABLE tb_Production
(ProductNo
   CHAR(6)
   NOT NULL
   CONSTRAINT pk_Production_ProductNo
   PRIMARY KEY(ProductNo)
   CONSTRAINT ck_Production_ProductNo
   CHECK(ProductNo LIKE '[0-9][0-9][0-9][0-9][0-9][0-9]')
,Name
 		VARCHAR(20) 								
		NOT NULL	
,Addr
        VARCHAR(50)
		NOT NULL
,Tel
        CHAR(11)
		NOT NULL
,Email
        CHAR(50)
,MedicineNo
        CHAR(10)
		NOT NULL
		CONSTRAINT fk_Production_MedicineNo				
	       FOREIGN KEY(MedicineNo)				
	       REFERENCES tb_Medicine(MedicineNo) 	
,Contactor
        VARCHAR(10)
		NOT NULL																												
)
IF OBJECT_ID ('tb_Seller') IS NOT NULL
DROP TABLE tb_Seller;
GO 
CREATE TABLE tb_Seller
(SellerNo
   CHAR(8)
   NOT NULL
   CONSTRAINT pk_Seller_SellerNo
   PRIMARY KEY(SellerNo)
   CONSTRAINT ck_Seller_SellerNo
   CHECK(SellerNo LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
,Name
 		VARCHAR(20) 								
		NOT NULL								
,Addr
        VARCHAR(50)
		NOT NULL
,Tel
        char(11)
        NOT NULL
,Email
        CHAR(50)
,MedicineNo
        char(10)
		NOT NULL
		CONSTRAINT fk_Seller_MedicineNo					
	       FOREIGN KEY(MedicineNo)				
	       REFERENCES tb_Medicine(MedicineNo)
,ProductNo
  char(6)
		NOT NULL
		CONSTRAINT fk_Seller_ProductNo				
	       FOREIGN KEY(ProductNo)				
	       REFERENCES tb_Production(ProductNo)
,GYConstractor
       VARCHAR(10)
	   NOT NULL
,HConstractor
       CHAR(4)
	   NOT NULL
	    CONSTRAINT ck_Seller_HConstractor
   CHECK(HConstractor LIKE '[A-Z][0-9][0-9][0-9]')																											
)