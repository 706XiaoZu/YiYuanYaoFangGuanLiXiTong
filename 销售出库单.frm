VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "销售出货单"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   10935
   ScaleWidth      =   20250
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command18 
      Caption         =   "调阅凭证"
      Enabled         =   0   'False
      Height          =   735
      Left            =   15600
      TabIndex        =   68
      Top             =   9840
      Width           =   1335
   End
   Begin VB.CommandButton Command17 
      Caption         =   "结算历程"
      Enabled         =   0   'False
      Height          =   615
      Left            =   13800
      TabIndex        =   67
      Top             =   9960
      Width           =   1335
   End
   Begin VB.CommandButton Command16 
      Caption         =   "取消审核"
      Enabled         =   0   'False
      Height          =   735
      Left            =   15600
      TabIndex        =   66
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton Command15 
      Caption         =   "审核通过"
      Height          =   735
      Left            =   13800
      TabIndex        =   65
      Top             =   8880
      Width           =   1335
   End
   Begin VB.TextBox Text18 
      Height          =   495
      Left            =   11640
      TabIndex        =   64
      Top             =   9960
      Width           =   1695
   End
   Begin VB.TextBox Text17 
      Height          =   495
      Left            =   11640
      TabIndex        =   63
      Top             =   9120
      Width           =   1695
   End
   Begin VB.TextBox Text16 
      Height          =   495
      Left            =   10320
      TabIndex        =   59
      Top             =   8040
      Width           =   495
   End
   Begin VB.TextBox Text15 
      Height          =   495
      Left            =   4440
      TabIndex        =   58
      Top             =   10200
      Width           =   2895
   End
   Begin VB.TextBox Text14 
      Height          =   495
      Left            =   7800
      TabIndex        =   57
      Top             =   9480
      Width           =   1815
   End
   Begin VB.TextBox Text13 
      Height          =   495
      Left            =   7800
      TabIndex        =   56
      Top             =   8880
      Width           =   1815
   End
   Begin VB.TextBox Text12 
      Height          =   495
      Left            =   5280
      TabIndex        =   53
      Top             =   9480
      Width           =   1455
   End
   Begin VB.TextBox Text11 
      Height          =   495
      Left            =   5280
      TabIndex        =   52
      Top             =   8880
      Width           =   1455
   End
   Begin VB.TextBox Text10 
      Height          =   615
      Left            =   4320
      TabIndex        =   49
      Top             =   8040
      Width           =   5295
   End
   Begin VB.ComboBox Combo8 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15.75
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   1560
      TabIndex        =   48
      Top             =   10320
      Width           =   2655
   End
   Begin VB.TextBox Text9 
      Height          =   495
      Left            =   1560
      TabIndex        =   47
      Top             =   9600
      Width           =   2655
   End
   Begin VB.TextBox Text8 
      Height          =   495
      Left            =   1560
      TabIndex        =   46
      Top             =   8880
      Width           =   2655
   End
   Begin VB.TextBox Text7 
      Height          =   615
      Left            =   1560
      TabIndex        =   45
      Top             =   8040
      Width           =   2655
   End
   Begin VB.CommandButton Command14 
      Caption         =   "参考"
      Height          =   615
      Left            =   17640
      TabIndex        =   40
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton Command13 
      Caption         =   "导入"
      Height          =   615
      Left            =   16080
      TabIndex        =   39
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton Command12 
      Caption         =   "补货"
      Height          =   615
      Left            =   14520
      TabIndex        =   38
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton Command11 
      Caption         =   "处方"
      Height          =   615
      Left            =   12960
      TabIndex        =   37
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton Command10 
      Caption         =   "拆零"
      Height          =   615
      Left            =   11400
      TabIndex        =   36
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton Command9 
      Caption         =   "消费"
      Height          =   615
      Left            =   9720
      TabIndex        =   35
      Top             =   3600
      Width           =   1215
   End
   Begin VB.CommandButton Command8 
      Caption         =   "热键"
      Height          =   615
      Left            =   8160
      TabIndex        =   34
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton Command7 
      Caption         =   "表栏"
      Height          =   615
      Left            =   6600
      TabIndex        =   33
      Top             =   3600
      Width           =   1095
   End
   Begin VB.TextBox Text6 
      Height          =   2295
      Left            =   16560
      TabIndex        =   32
      Top             =   960
      Width           =   3735
   End
   Begin VB.TextBox Text5 
      Height          =   495
      Left            =   13440
      TabIndex        =   30
      Top             =   2760
      Width           =   1695
   End
   Begin VB.ComboBox Combo7 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15.75
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   13440
      TabIndex        =   29
      Text            =   "收据"
      Top             =   1920
      Width           =   1695
   End
   Begin VB.ComboBox Combo6 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15.75
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8280
      TabIndex        =   22
      Top             =   2760
      Width           =   2055
   End
   Begin VB.ComboBox Combo5 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15.75
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8280
      TabIndex        =   21
      Top             =   1920
      Width           =   2055
   End
   Begin VB.TextBox Text4 
      Height          =   615
      Left            =   3720
      TabIndex        =   18
      Top             =   2760
      Width           =   2655
   End
   Begin VB.TextBox Text3 
      Height          =   615
      Left            =   3720
      TabIndex        =   17
      Top             =   1920
      Width           =   2655
   End
   Begin VB.TextBox Text2 
      Height          =   615
      Left            =   3720
      TabIndex        =   16
      Top             =   1080
      Width           =   6615
   End
   Begin VB.ComboBox Combo4 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1800
      TabIndex        =   15
      Top             =   2880
      Width           =   1695
   End
   Begin VB.ComboBox Combo3 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1800
      TabIndex        =   14
      Top             =   2040
      Width           =   1695
   End
   Begin VB.ComboBox Combo2 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1800
      TabIndex        =   13
      Top             =   1200
      Width           =   1695
   End
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   15.75
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   13560
      TabIndex        =   9
      Text            =   "2017年10月15日"
      Top             =   240
      Width           =   2775
   End
   Begin VB.TextBox Text1 
      Height          =   615
      Left            =   9480
      TabIndex        =   7
      Top             =   120
      Width           =   2175
   End
   Begin VB.CommandButton Command6 
      Caption         =   "退出"
      Height          =   615
      Left            =   6480
      TabIndex        =   5
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton Command5 
      Caption         =   "打印"
      Height          =   615
      Left            =   5160
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton Command4 
      Caption         =   "导出"
      Height          =   615
      Left            =   3840
      TabIndex        =   3
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton Command3 
      Caption         =   "删除"
      Height          =   615
      Left            =   2520
      TabIndex        =   2
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton Command2 
      Caption         =   "新增"
      Height          =   615
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Width           =   855
   End
   Begin VB.CommandButton Command1 
      Caption         =   "保存"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label25 
      Caption         =   "审核人："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   10320
      TabIndex        =   62
      Top             =   9960
      Width           =   1215
   End
   Begin VB.Label Label24 
      Caption         =   "制单人："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10320
      TabIndex        =   61
      Top             =   9120
      Width           =   1215
   End
   Begin VB.Label Label23 
      Caption         =   "使用当前客户最近一次购物价格"
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11040
      TabIndex        =   60
      Top             =   8040
      Width           =   4335
   End
   Begin VB.Label Label22 
      Caption         =   "找零："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   55
      Top             =   9600
      Width           =   1095
   End
   Begin VB.Label Label21 
      Caption         =   "应收："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   54
      Top             =   8880
      Width           =   1095
   End
   Begin VB.Label Label20 
      Caption         =   "收款："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   51
      Top             =   9600
      Width           =   1095
   End
   Begin VB.Label Label19 
      Caption         =   "调减："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   50
      Top             =   8880
      Width           =   1095
   End
   Begin VB.Label Label18 
      Caption         =   "结算方式："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   44
      Top             =   10320
      Width           =   1695
   End
   Begin VB.Label Label17 
      Caption         =   "本次现收："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   43
      Top             =   9600
      Width           =   1695
   End
   Begin VB.Label Label16 
      Caption         =   "总合计："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   42
      Top             =   8880
      Width           =   1695
   End
   Begin VB.Label Label15 
      Caption         =   "药品条码："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   41
      Top             =   8160
      Width           =   1695
   End
   Begin VB.Label Label14 
      Caption         =   "备注："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   15720
      TabIndex        =   31
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label13 
      Caption         =   "票据号码："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   11640
      TabIndex        =   28
      Top             =   2760
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "票据类型："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   11640
      TabIndex        =   27
      Top             =   1920
      Width           =   1575
   End
   Begin VB.Label Label11 
      Height          =   495
      Left            =   13920
      TabIndex        =   26
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label10 
      Height          =   495
      Left            =   12120
      TabIndex        =   25
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label9 
      Height          =   495
      Left            =   10560
      TabIndex        =   24
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "天"
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10560
      TabIndex        =   23
      Top             =   2760
      Width           =   495
   End
   Begin VB.Label Label7 
      Caption         =   "收款期限："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      TabIndex        =   20
      Top             =   2760
      Width           =   1695
   End
   Begin VB.Label Label6 
      Caption         =   "销售类型："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      TabIndex        =   19
      Top             =   1920
      Width           =   1695
   End
   Begin VB.Label Label5 
      Caption         =   "业务员："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   12
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "部 门："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   11
      Top             =   2040
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "客 户："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   10
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "日期："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   12240
      TabIndex        =   8
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "单号："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   6
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command2_Click()

End Sub
