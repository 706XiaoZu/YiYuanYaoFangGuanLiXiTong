VERSION 5.00
Begin VB.Form Form3 
   Caption         =   "Form3"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form3"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   StartUpPosition =   3  '窗口缺省
   Begin VB.Image Image9 
      Height          =   1335
      Left            =   15720
      Stretch         =   -1  'True
      Top             =   4200
      Width           =   1935
   End
   Begin VB.Image Image8 
      Height          =   1335
      Left            =   15720
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Image Image7 
      Height          =   1455
      Left            =   9960
      Stretch         =   -1  'True
      Top             =   8520
      Width           =   2295
   End
   Begin VB.Image Image6 
      Height          =   1095
      Left            =   12360
      Stretch         =   -1  'True
      Top             =   3720
      Width           =   2055
   End
   Begin VB.Image Image5 
      Height          =   1095
      Left            =   12600
      Stretch         =   -1  'True
      Top             =   360
      Width           =   1815
   End
   Begin VB.Image Image4 
      Height          =   1215
      Left            =   1200
      Stretch         =   -1  'True
      Top             =   7080
      Width           =   1935
   End
   Begin VB.Image Image3 
      Height          =   1095
      Left            =   2880
      Stretch         =   -1  'True
      Top             =   3720
      Width           =   1815
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   1080
      Stretch         =   -1  'True
      Top             =   480
      Width           =   1935
   End
   Begin VB.Image Image1 
      Height          =   10935
      Left            =   0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   20295
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Form3.Left = 0
Form3.Top = 0
Form3.Width = Screen.Width
Form3.Height = Screen.Height
Form3.Scale (0, 0)-(Form3.Width, Form3.Height)
End Sub

Private Sub Form_Resize()
Form3.Image1.Height = Form3.Height
Form3.Image1.Width = Form3.Width
Form3.Image1.Picture = LoadPicture(App.Path & "\" & "基础资料背景.jpg")
Form3.Image2.Picture = LoadPicture(App.Path & "\" & "部门资料.gif")
Form3.Image3.Picture = LoadPicture(App.Path & "\" & "员工资料.gif")
Form3.Image4.Picture = LoadPicture(App.Path & "\" & "地区资料.gif")
Form3.Image5.Picture = LoadPicture(App.Path & "\" & "药品资料.gif")
Form3.Image6.Picture = LoadPicture(App.Path & "\" & "供应商资料.gif")
Form3.Image7.Picture = LoadPicture(App.Path & "\" & "基本设置.gif")
Form3.Image8.Picture = LoadPicture(App.Path & "\" & "仓库管理.gif")
Form3.Image9.Picture = LoadPicture(App.Path & "\" & "期初管理.gif")
End Sub

Private Sub Image7_Click()
Form3.Hide
Form2.Show
End Sub
