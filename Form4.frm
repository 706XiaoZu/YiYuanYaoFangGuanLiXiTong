VERSION 5.00
Begin VB.Form Form4 
   Caption         =   "Form4"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form4"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   StartUpPosition =   3  '窗口缺省
   Begin VB.Image Image6 
      Height          =   855
      Left            =   9840
      Stretch         =   -1  'True
      Top             =   7680
      Width           =   3855
   End
   Begin VB.Image Image5 
      Height          =   855
      Left            =   11640
      Stretch         =   -1  'True
      Top             =   5760
      Width           =   4095
   End
   Begin VB.Image Image4 
      Height          =   855
      Left            =   9240
      Stretch         =   -1  'True
      Top             =   3840
      Width           =   4215
   End
   Begin VB.Image Image3 
      Height          =   855
      Left            =   11640
      Stretch         =   -1  'True
      Top             =   2280
      Width           =   3975
   End
   Begin VB.Image Image2 
      Height          =   855
      Left            =   9600
      Stretch         =   -1  'True
      Top             =   480
      Width           =   3975
   End
   Begin VB.Image Image1 
      Height          =   10935
      Left            =   0
      Stretch         =   -1  'True
      Top             =   -1320
      Width           =   20295
   End
End
Attribute VB_Name = "Form4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Form4.Left = 0
Form4.Top = 0
Form4.Width = Screen.Width
Form4.Height = Screen.Height
Form4.Scale (0, 0)-(Form4.Width, Form4.Height)
End Sub

Private Sub Form_Resize()
Form4.Image1.Height = Form4.Height
Form4.Image1.Width = Form4.Width
Form4.Image1.Picture = LoadPicture(App.Path & "\" & "销售管理界面背景.jpg")
Form4.Image2.Picture = LoadPicture(App.Path & "\" & "销售出库单.gif")
Form4.Image3.Picture = LoadPicture(App.Path & "\" & "销售退货单.gif")
Form4.Image4.Picture = LoadPicture(App.Path & "\" & "近效期提示单.gif")
Form4.Image5.Picture = LoadPicture(App.Path & "\" & "销售库存报警.gif")
Form4.Image6.Picture = LoadPicture(App.Path & "\" & "销售特批.gif")
End Sub


