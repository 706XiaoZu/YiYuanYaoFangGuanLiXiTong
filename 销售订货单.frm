VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "销售订货单"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   10935
   ScaleWidth      =   20250
   StartUpPosition =   3  '窗口缺省
   Begin VB.ComboBox Combo5 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15.75
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   13560
      TabIndex        =   41
      Text            =   "2017年10月15日"
      Top             =   240
      Width           =   2775
   End
   Begin VB.TextBox Text11 
      Height          =   615
      Left            =   10200
      TabIndex        =   39
      Top             =   120
      Width           =   1815
   End
   Begin VB.CommandButton Command11 
      Caption         =   "退出"
      Height          =   615
      Left            =   7560
      TabIndex        =   37
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton Command10 
      Caption         =   "打印"
      Height          =   615
      Left            =   6120
      TabIndex        =   36
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton Command9 
      Caption         =   "导出"
      Height          =   615
      Left            =   4680
      TabIndex        =   35
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton Command8 
      Caption         =   "删除"
      Height          =   615
      Left            =   3240
      TabIndex        =   34
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton Command7 
      Caption         =   "新增"
      Height          =   615
      Left            =   1800
      TabIndex        =   33
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton Command6 
      Caption         =   "保存"
      Height          =   615
      Left            =   360
      TabIndex        =   32
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton Command5 
      Caption         =   "生成补货单（B）"
      Enabled         =   0   'False
      Height          =   735
      Left            =   15120
      TabIndex        =   31
      Top             =   9720
      Width           =   1575
   End
   Begin VB.CommandButton Command4 
      Caption         =   "调阅凭证（L）"
      Enabled         =   0   'False
      Height          =   735
      Left            =   13320
      TabIndex        =   30
      Top             =   9720
      Width           =   1575
   End
   Begin VB.CommandButton Command3 
      Caption         =   "结算历程（H)"
      Enabled         =   0   'False
      Height          =   735
      Left            =   11400
      TabIndex        =   29
      Top             =   9720
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "取消审核（Q）"
      Enabled         =   0   'False
      Height          =   735
      Left            =   9600
      TabIndex        =   28
      Top             =   9720
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "审核通过（T)"
      Height          =   735
      Left            =   7560
      TabIndex        =   27
      Top             =   9720
      Width           =   1815
   End
   Begin VB.TextBox Text10 
      Height          =   495
      Left            =   5040
      TabIndex        =   26
      Top             =   9360
      Width           =   1695
   End
   Begin VB.TextBox Text9 
      Height          =   495
      Left            =   3720
      TabIndex        =   24
      Top             =   10200
      Width           =   3015
   End
   Begin VB.ComboBox Combo4 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1920
      TabIndex        =   23
      Top             =   10200
      Width           =   1455
   End
   Begin VB.TextBox Text8 
      Height          =   495
      Left            =   1680
      TabIndex        =   22
      Top             =   9360
      Width           =   1695
   End
   Begin VB.TextBox Text7 
      Height          =   2295
      Left            =   15840
      TabIndex        =   19
      Top             =   1080
      Width           =   4455
   End
   Begin VB.TextBox Text6 
      Height          =   495
      Left            =   12720
      TabIndex        =   17
      Top             =   2880
      Width           =   1935
   End
   Begin VB.TextBox Text5 
      Height          =   495
      Left            =   12720
      TabIndex        =   16
      Top             =   2160
      Width           =   1935
   End
   Begin VB.TextBox Text4 
      Height          =   495
      Left            =   7800
      TabIndex        =   10
      Top             =   2880
      Width           =   1695
   End
   Begin VB.TextBox Text3 
      Height          =   495
      Left            =   3600
      TabIndex        =   8
      Top             =   2880
      Width           =   2295
   End
   Begin VB.TextBox Text2 
      Height          =   495
      Left            =   3600
      TabIndex        =   7
      Top             =   2040
      Width           =   5895
   End
   Begin VB.TextBox Text1 
      Height          =   495
      Left            =   3600
      TabIndex        =   6
      Top             =   1200
      Width           =   5895
   End
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1560
      TabIndex        =   5
      Top             =   1200
      Width           =   1695
   End
   Begin VB.ComboBox Combo3 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1560
      TabIndex        =   4
      Top             =   2880
      Width           =   1695
   End
   Begin VB.ComboBox Combo2 
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1560
      TabIndex        =   3
      Top             =   2040
      Width           =   1695
   End
   Begin VB.Label Label15 
      Caption         =   "日期："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12240
      TabIndex        =   40
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label14 
      Caption         =   "单号："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9120
      TabIndex        =   38
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label13 
      Caption         =   "预收："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      TabIndex        =   25
      Top             =   9360
      Width           =   975
   End
   Begin VB.Label Label12 
      Caption         =   "结算方式："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   21
      Top             =   10200
      Width           =   1575
   End
   Begin VB.Label Label11 
      Caption         =   "总合计："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   9360
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "备注："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   15000
      TabIndex        =   18
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label9 
      Caption         =   "审核人"
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11400
      TabIndex        =   15
      Top             =   3000
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "制单人"
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11400
      TabIndex        =   14
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label7 
      Height          =   495
      Left            =   13080
      TabIndex        =   13
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label Label6 
      Height          =   495
      Left            =   11400
      TabIndex        =   12
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label Label5 
      Height          =   495
      Left            =   9720
      TabIndex        =   11
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "销售类型"
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   9
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "业务员"
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   2880
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "部门"
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   1
      Top             =   2040
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "客户："
      BeginProperty Font 
         Name            =   "幼圆"
         Size            =   15
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   0
      Top             =   1200
      Width           =   615
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Label1_Click()

End Sub

Private Sub Label3_Click()

End Sub

Private Sub Text2_Change()

End Sub
