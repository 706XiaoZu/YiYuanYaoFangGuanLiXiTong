VERSION 5.00
Begin VB.Form Form2 
   Caption         =   "Form2"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form2"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   StartUpPosition =   3  '窗口缺省
   Begin VB.Image Image8 
      Height          =   735
      Left            =   15480
      Stretch         =   -1  'True
      Top             =   8520
      Width           =   1935
   End
   Begin VB.Image Image7 
      Height          =   1215
      Left            =   9600
      Stretch         =   -1  'True
      Top             =   7320
      Width           =   2535
   End
   Begin VB.Image Image6 
      Height          =   1215
      Left            =   5640
      Stretch         =   -1  'True
      Top             =   7320
      Width           =   2655
   End
   Begin VB.Image Image5 
      Height          =   1215
      Left            =   10680
      Stretch         =   -1  'True
      Top             =   4800
      Width           =   2415
   End
   Begin VB.Image Image4 
      Height          =   1215
      Left            =   6600
      Stretch         =   -1  'True
      Top             =   4800
      Width           =   2775
   End
   Begin VB.Image Image3 
      Height          =   1215
      Left            =   11640
      Stretch         =   -1  'True
      Top             =   2520
      Width           =   2415
   End
   Begin VB.Image Image2 
      Height          =   1215
      Left            =   7800
      Stretch         =   -1  'True
      Top             =   2520
      Width           =   2655
   End
   Begin VB.Image Image1 
      Height          =   11055
      Left            =   0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   20295
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Form2.Left = 0
Form2.Top = 0
Form2.Width = Screen.Width
Form2.Height = Screen.Height
Form2.Scale (0, 0)-(Form2.Width, Form2.Height)
End Sub

Private Sub Form_Resize()
Form2.Image1.Height = Form2.Height
Form2.Image1.Width = Form2.Width
Form2.Image1.Picture = LoadPicture(App.Path & "\" & "菜单背景.jpg")
Form2.Image2.Picture = LoadPicture(App.Path & "\" & "基础资料.gif")
Form2.Image3.Picture = LoadPicture(App.Path & "\" & "库存管理.gif")
Form2.Image4.Picture = LoadPicture(App.Path & "\" & "销售管理.gif")
Form2.Image5.Picture = LoadPicture(App.Path & "\" & "常用工具.gif")
Form2.Image6.Picture = LoadPicture(App.Path & "\" & "系统管理.gif")
Form2.Image7.Picture = LoadPicture(App.Path & "\" & "使用说明.gif")
Form2.Image8.Picture = LoadPicture(App.Path & "\" & "登录用户.gif")

End Sub


Private Sub Image2_Click()
Form2.Hide
Form3.Show

End Sub

Private Sub Image4_Click()
Form2.Hide
Form4.Show

End Sub
