VERSION 5.00
Begin VB.Form 辅助功能_前台销售 
   Caption         =   "Form5"
   ClientHeight    =   8355
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   15690
   BeginProperty Font 
      Name            =   "幼圆"
      Size            =   15
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form5"
   ScaleHeight     =   8355
   ScaleWidth      =   15690
   StartUpPosition =   3  '窗口缺省
   Begin VB.TextBox Text13 
      BorderStyle     =   0  'None
      Height          =   1935
      Left            =   12960
      MultiLine       =   -1  'True
      TabIndex        =   37
      Text            =   "Form5.frx":0000
      Top             =   480
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   600
      TabIndex        =   19
      Top             =   5880
      Width           =   15135
      Begin VB.CommandButton Command4 
         Caption         =   "结算（J）"
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15.75
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   12840
         TabIndex        =   35
         Top             =   1320
         Width           =   1935
      End
      Begin VB.CommandButton Command3 
         Caption         =   "打印（P）"
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15.75
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   10440
         TabIndex        =   34
         Top             =   1320
         Width           =   2055
      End
      Begin VB.CommandButton Command2 
         Caption         =   "挂单（G）"
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15.75
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   12840
         TabIndex        =   33
         Top             =   360
         Width           =   1935
      End
      Begin VB.CommandButton Command1 
         Caption         =   "落单（L）"
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15.75
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   10440
         TabIndex        =   32
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox Text12 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6720
         TabIndex        =   31
         Top             =   1680
         Width           =   3015
      End
      Begin VB.TextBox Text11 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6720
         TabIndex        =   30
         Top             =   960
         Width           =   3015
      End
      Begin VB.TextBox Text10 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6720
         TabIndex        =   29
         Top             =   240
         Width           =   3015
      End
      Begin VB.TextBox Text9 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1200
         TabIndex        =   25
         Top             =   1680
         Width           =   3015
      End
      Begin VB.TextBox Text8 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1200
         TabIndex        =   23
         Top             =   960
         Width           =   3015
      End
      Begin VB.TextBox Text7 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1200
         TabIndex        =   21
         Top             =   240
         Width           =   3015
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "前单找零;"
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5040
         TabIndex        =   28
         Top             =   1680
         Width           =   1425
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "前单收款："
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5040
         TabIndex        =   27
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "前单应收："
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5040
         TabIndex        =   26
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "应收："
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   240
         TabIndex        =   24
         Top             =   1680
         Width           =   945
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "调减："
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   240
         TabIndex        =   22
         Top             =   960
         Width           =   945
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "总计："
         BeginProperty Font 
            Name            =   "幼圆"
            Size            =   15
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   240
         TabIndex        =   20
         Top             =   240
         Width           =   945
      End
   End
   Begin VB.CheckBox Check3 
      Caption         =   "检索加速"
      Height          =   450
      Left            =   8640
      TabIndex        =   18
      Top             =   4560
      Width           =   1815
   End
   Begin VB.CheckBox Check2 
      Caption         =   "使用当前客户最近一次购买价格"
      Height          =   495
      Left            =   6360
      TabIndex        =   17
      Top             =   5160
      Width           =   4695
   End
   Begin VB.CheckBox Check1 
      Caption         =   "语音报价"
      Height          =   375
      Left            =   6360
      TabIndex        =   16
      Top             =   4560
      Width           =   1695
   End
   Begin VB.TextBox Text6 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   3840
      TabIndex        =   15
      Top             =   5160
      Width           =   1815
   End
   Begin VB.ComboBox Combo3 
      Height          =   420
      Left            =   2280
      TabIndex        =   14
      Top             =   5160
      Width           =   1215
   End
   Begin VB.TextBox Text5 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   2280
      TabIndex        =   12
      Top             =   4440
      Width           =   3375
   End
   Begin VB.ComboBox Combo2 
      Height          =   420
      ItemData        =   "Form5.frx":0012
      Left            =   9720
      List            =   "Form5.frx":001C
      TabIndex        =   10
      Text            =   "零售"
      Top             =   480
      Width           =   1095
   End
   Begin VB.TextBox Text4 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   4680
      TabIndex        =   8
      Text            =   "普通顾客1"
      Top             =   1200
      Width           =   2175
   End
   Begin VB.TextBox Text3 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   1680
      TabIndex        =   7
      Top             =   1920
      Width           =   9855
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   5640
      TabIndex        =   5
      Top             =   360
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   1680
      TabIndex        =   3
      Top             =   360
      Width           =   2535
   End
   Begin VB.ComboBox Combo1 
      Height          =   420
      Left            =   1680
      TabIndex        =   2
      Top             =   1200
      Width           =   2535
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      Caption         =   "信息："
      Height          =   300
      Left            =   11760
      TabIndex        =   36
      Top             =   480
      Width           =   900
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "营 业 员："
      Height          =   300
      Left            =   600
      TabIndex        =   13
      Top             =   5160
      Width           =   1500
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "药品条码："
      Height          =   300
      Left            =   600
      TabIndex        =   11
      Top             =   4560
      Width           =   1500
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "销售类型："
      Height          =   300
      Left            =   8040
      TabIndex        =   9
      Top             =   480
      Width           =   1500
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "备注："
      Height          =   300
      Left            =   480
      TabIndex        =   6
      Top             =   2040
      Width           =   900
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "日期："
      Height          =   300
      Left            =   4680
      TabIndex        =   4
      Top             =   480
      Width           =   900
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "客户："
      Height          =   420
      Left            =   480
      TabIndex        =   1
      Top             =   1200
      Width           =   900
   End
   Begin VB.Label 单号 
      AutoSize        =   -1  'True
      Caption         =   "单号："
      Height          =   420
      Left            =   480
      TabIndex        =   0
      Top             =   480
      Width           =   900
   End
End
Attribute VB_Name = "辅助功能_前台销售"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check3_Click()

End Sub

Private Sub Form_Load()

End Sub

Private Sub Label5_Click()

End Sub

Private Sub Text11_Change()

End Sub
